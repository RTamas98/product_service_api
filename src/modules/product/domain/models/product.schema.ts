import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IProduct } from './interfaces/product.interface';
import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

@Schema()
export class Product implements IProduct {
    @ApiProperty()
    @IsString()
    @Prop({ type: String, required: true })
    details: string;

    @ApiProperty()
    @IsString()
    @Prop({ type: String, required: true })
    image: string;

    @ApiProperty()
    @IsString()
    @Prop({ type: String, required: true })
    name: string;

    @ApiProperty()
    @IsString()
    @Prop({ type: String, required: true })
    price: string;

    @ApiProperty()
    @Prop({ type: String, required: true })
    discount: string;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
