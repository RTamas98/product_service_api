import { Module } from '@nestjs/common';
import { ProductController } from './infrastructure/controllers/product.controller';
import { ProductService } from './domain/services/product.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Product, ProductSchema } from './domain/models/product.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: Product.name, schema: ProductSchema }])],
    providers: [ProductService],
    controllers: [ProductController],
})
export class ProductModule {}
