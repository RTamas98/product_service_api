export interface IProduct {
    image: string;
    name: string;
    price: string;
    details: string;
    discount: string;
}
