import { Injectable, Logger } from '@nestjs/common';
import { Product } from '../models/product.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as data from './MOCK_DATA.json';

@Injectable()
export class ProductService {
    logger = new Logger(Product.name);
    constructor(@InjectModel(Product.name) private readonly productModel: Model<Product>) {}

    list(limit, skip) {
        this.logger.log(`The limit is ${limit} and the skip is ${skip}.`);
        return this.productModel.find({}).limit(limit).skip(skip);
    }

    create() {
        return this.productModel.create(data);
    }
}
