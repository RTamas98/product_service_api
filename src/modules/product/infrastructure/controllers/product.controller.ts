import { ApiOkResponse, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Controller, Get, Post, Query } from '@nestjs/common';
import { ProductService } from '../../domain/services/product.service';

@ApiTags('Product')
@Controller('products')
export class ProductController {
    constructor(private readonly productService: ProductService) {}

    @Get()
    @ApiOkResponse({ description: 'List all products, you can set the limit and the skip, from 0.' })
    @ApiQuery({ name: 'limit', type: Number, required: false, description: 'Default value is 10.' })
    @ApiQuery({ name: 'skip', type: Number, required: false, description: 'Default value is 0.' })
    list(@Query('limit') limit = 10, @Query('skip') skip = 0) {
        return this.productService.list(limit, skip);
    }

    @Post()
    @ApiOkResponse({ description: 'Creates 100 fake data.' })
    create() {
        return this.productService.create();
    }
}
